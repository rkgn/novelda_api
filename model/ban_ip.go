package model

import (
	"time"

	"github.com/jackc/pgtype"
)

type BANIP struct {
	ID        uint        `gorm:"primaryKey;autoIncrement"`
	IP        pgtype.Inet `gorm:"type:inet"`
	FailedCnt uint
	FailedAt  time.Time
}
