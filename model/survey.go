package model

import (
	"time"
)

type Survey struct {
	ID        uint `gorm:"primaryKey;autoIncrement"`
	Name      string
	Json      string
	CreatedAt time.Time
}
