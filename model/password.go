package model

import (
  "time"
)

type Password struct {
  ID uint `gorm:"primaryKey;autoIncrement"`
  PasswordHash string
  CreatedAt time.Time
  TTL uint
}
