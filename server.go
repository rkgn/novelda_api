package main

import (
	"flag"
	"fmt"
	"net/http"
	"novelda_api/model"
	"strconv"
	"time"

	"log"
	"os"

	"github.com/alexedwards/argon2id"
	"github.com/gorilla/sessions"
	"github.com/jackc/pgtype"
	"github.com/joho/godotenv"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type survey_request struct {
	Password string `json:"password"`
	Name     string `json:"name"`
	Survey   string `json:"survey"`
}

type survey_pw_request struct {
	Password string `json:"password"`
	TTL      uint   `json:"ttl"`
}

type auth_request struct {
	UID      string `json:"uid"`
	Password string `json:"password"`
}
type ban_ips_res struct {
	ID  uint   `json:"id"`
	IP  string `json:"ip"`
	Cnt uint   `json:"cnt"`
}
type survey_res struct {
	ID     uint   `json:"id"`
	Name   string `json:"name"`
	Time   string `json:"time"`
	Survey string `json:"survey"`
}

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("loading .env failed")
	}
	flag.Parse()
	if len(flag.Args()) == 0 {
		env := os.Getenv("ENV")
		is_cookie_secure := env == "production"
		frontend_origin := os.Getenv("FRONTEND_ORIGIN")
		e := echo.New()
		model.Connect()
		e.Use(middleware.Recover())
		e.Use(middleware.Logger())
		e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins:     []string{frontend_origin},
			AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAccessControlAllowOrigin},
			AllowMethods:     []string{http.MethodGet, http.MethodPost, http.MethodDelete},
			AllowCredentials: true,
		}))
		e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
			TokenLookup:    "cookie:_csrf",
			CookiePath:     "/",
			CookieSecure:   is_cookie_secure,
			CookieHTTPOnly: true,
			CookieSameSite: http.SameSiteStrictMode,
		}))
		e.Use(session.Middleware(sessions.NewCookieStore([]byte("secret"))))
		e.Use(rejectBannedIP)
		e.GET("/is_admin", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				return c.NoContent(http.StatusOK)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.POST("/add_survey_password", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				r := new(survey_pw_request)
				if err := c.Bind(r); err != nil {
					return c.NoContent(http.StatusBadRequest)
				}
				hash, _ := argon2id.CreateHash(r.Password, argon2id.DefaultParams)
				_ = model.DB.Omit("ID").Create(&model.Password{PasswordHash: hash, CreatedAt: time.Now(), TTL: r.TTL})
				return c.NoContent(http.StatusCreated)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.POST("/signin", func(c echo.Context) error {
			r := new(auth_request)
			var admin model.Admin
			if err := c.Bind(r); err != nil {
				ban(c.RealIP())
				return c.NoContent(http.StatusBadRequest)
			}
			err := model.DB.Where("uid = ?", r.UID).First(&admin).Error
			if err != nil {
				ban(c.RealIP())
				return c.NoContent(http.StatusForbidden)
			}
			is_pw_match, _ := argon2id.ComparePasswordAndHash(r.Password, admin.PasswordHash)
			if is_pw_match {
				var banned_ip model.BANIP
				var inet pgtype.Inet
				inet.Set(c.RealIP())
				err := model.DB.Where("ip = ?", inet).First(&banned_ip).Error
				if err == nil {
					model.DB.Delete(&banned_ip)
				}
				sess, _ := session.Get("session", c)
				sess.Options = &sessions.Options{
					Path:     "/",
					MaxAge:   604800,
					Secure:   is_cookie_secure,
					HttpOnly: true,
					SameSite: http.SameSiteStrictMode,
				}
				sess.Values["admin"] = r.UID
				sess.Save(c.Request(), c.Response())
				return c.NoContent(http.StatusOK)
			}
			ban(c.RealIP())
			return c.NoContent(http.StatusForbidden)
		})
		e.GET("/signout", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				sess.Values["admin"] = nil
				sess.Save(c.Request(), c.Response())
				return c.NoContent(http.StatusNoContent)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.GET("/", func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		})
		e.GET("/surveys", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				var surveys []model.Survey
				var res []survey_res
				model.DB.Find(&surveys)
				for _, v := range surveys {
					var tmp_res survey_res
					tmp_res.ID = v.ID
					tmp_res.Name = v.Name
					tmp_res.Time = v.CreatedAt.Format("2006-01-02T15:04:05Z07:00")
					tmp_res.Survey = v.Json
					res = append(res, tmp_res)
				}
				if len(res) == 0 {
					return c.JSON(http.StatusOK, []string{})
				}
				return c.JSON(http.StatusOK, res)
			} else {
				return c.NoContent(http.StatusForbidden)
			}

		})
		e.POST("/surveys", func(c echo.Context) error {
			r := new(survey_request)
			var passwords []model.Password
			result := model.DB.Find(&passwords)
			if result.Error != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			if err := c.Bind(r); err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
			for _, v := range passwords {
				is_pw_match, _ := argon2id.ComparePasswordAndHash(r.Password, v.PasswordHash)
				diff := time.Since(v.CreatedAt).Hours()
				if is_pw_match && diff < float64(v.TTL) {
					var surveys []model.Survey
					model.DB.Where("name = ?", r.Name).Find(&surveys)
					if len(surveys) != 0 {
						return c.NoContent(http.StatusConflict)
					}
					model.DB.Omit("ID").Create(&model.Survey{Name: r.Name, Json: r.Survey, CreatedAt: time.Now()})
					return c.NoContent(http.StatusCreated)
				}
			}
			return c.NoContent(http.StatusForbidden)
		})
		e.DELETE("/surveys/:id", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				id, err := strconv.Atoi(c.Param("id"))
				if err != nil {
					return c.NoContent(http.StatusBadRequest)
				}
				var survey model.Survey
				err = model.DB.Where("id = ?", id).First(&survey).Error
				if err != nil {
					return c.NoContent(http.StatusBadRequest)
				}
				model.DB.Delete(&survey)
				return c.NoContent(http.StatusNoContent)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.GET("/ban_ips", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				var ban_ips []model.BANIP
				var ips_res []ban_ips_res
				model.DB.Find(&ban_ips)
				for _, v := range ban_ips {
					var ip ban_ips_res
					ip.ID = v.ID
					ip.Cnt = v.FailedCnt
					ip.IP = v.IP.IPNet.String()
					ips_res = append(ips_res, ip)
				}
				if len(ban_ips) == 0 {
					return c.JSON(http.StatusOK, []string{})
				}
				return c.JSON(http.StatusOK, ips_res)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.DELETE("/ban_ips/:id", func(c echo.Context) error {
			sess, _ := session.Get("session", c)
			if sess.Values["admin"] != nil {
				id, err := strconv.Atoi(c.Param("id"))
				if err != nil {
					return c.NoContent(http.StatusBadRequest)
				}
				var ban_ip model.BANIP
				err = model.DB.Where("id = ?", id).First(&ban_ip).Error
				if err != nil {
					return c.NoContent(http.StatusBadRequest)
				}
				model.DB.Delete(&ban_ip)
				return c.NoContent(http.StatusNoContent)
			} else {
				return c.NoContent(http.StatusForbidden)
			}
		})
		e.Logger.Fatal(e.Start(":1323"))
	} else if flag.Arg(0) == "migrate" {
		model.Connect()
		model.Migrate()
	} else if flag.Arg(0) == "add_password" {
		var pw string
		fmt.Print("Enter password:")
		fmt.Scan(&pw)
		hash, _ := argon2id.CreateHash(pw, argon2id.DefaultParams)
		model.Connect()
		_ = model.DB.Omit("ID").Create(&model.Password{PasswordHash: hash, CreatedAt: time.Now(), TTL: 24})
	} else if flag.Arg(0) == "add_admin" {
		var uid, pw string
		var admins []model.Admin
		model.Connect()
		fmt.Print("Enter admin uid:")
		fmt.Scan(&uid)
		model.DB.Where("uid = ?", uid).Find(&admins)
		if len(admins) != 0 {
			fmt.Println("すでに使われているuidです")
		} else {
			fmt.Print("Enter admin password:")
			fmt.Scan(&pw)
			hash, _ := argon2id.CreateHash(pw, argon2id.DefaultParams)
			_ = model.DB.Create(&model.Admin{UID: uid, PasswordHash: hash})
		}
	}
}
func ban(ip string) {
	fmt.Println("banned IP-> " + ip)
	var banned_ip model.BANIP
	var inet pgtype.Inet
	inet.Set(ip)
	err := model.DB.Where("ip = ?", ip).First(&banned_ip).Error
	if err != nil {
		model.DB.Create(&model.BANIP{IP: inet, FailedCnt: 1, FailedAt: time.Now()})
	} else {
		banned_ip.FailedCnt++
		banned_ip.FailedAt = time.Now()
		model.DB.Save(&banned_ip)
	}
}

func rejectBannedIP(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var banned_ip model.BANIP
		var inet pgtype.Inet
		inet.Set(c.RealIP())
		err := model.DB.Where("ip = ?", inet).First(&banned_ip).Error
		if err == nil {
			cnt := banned_ip.FailedCnt
			diff := time.Since(banned_ip.FailedAt).Hours()
			if cnt == 3 && diff < 1 {
				return c.NoContent(http.StatusNotFound)
			} else if cnt == 4 && diff < 12 {
				return c.NoContent(http.StatusNotFound)
			} else if cnt > 4 {
				return c.NoContent(http.StatusNotFound)
			}
		}
		if err = next(c); err != nil {
			c.Error(err)
		}
		return nil
	}
}
